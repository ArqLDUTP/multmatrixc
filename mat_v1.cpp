#include <iostream>
#include <limits>
#include <vector>

using namespace std;

const int numNodes = 8;
const int INF = std::numeric_limits<int>::max();

const vector<vector<int>> convergenceMatrix = {
  // 0,  1,  2,  3,  4,  5,  6,  7
  {  0,  2,INF,  5,  2,INF,INF,INF},
  {INF,  0,INF,  6,INF,  4,  5,INF},
  {INF,  1,  0,INF,INF,INF,  7,INF},
  {INF,  8,INF,  0,INF,  6,INF,  4},
  {INF,INF,  1,INF,  0,INF,  9,INF},
  {INF,INF,INF,  6,INF,  0,INF,  8},
  {INF,INF,INF,INF,INF,INF,  0,  7},
  {INF,INF,INF,INF,INF,INF,  3,  0}
};

void getIdentMatrix(vector<vector<int>>& identMatrix) {
	for (int row = 0; row < numNodes; row++) {
		for (int col = 0; col < numNodes; col++) {
			if (row == col) {
				identMatrix[row][col] = 1;
			} else {
				identMatrix[row][col] = 0;
			}
		}
	}
}

int sum(int x, int y) {
  if ((x == INF) || (y == INF)) {
    return INF;
  } else {
    return x+y;
  }
}

template<typename T>
int dotProductMatrix(T& rowMatrix1, T& colMatrix2) {
	int resultValue = INF;

	for (int index = 0; index < numNodes; index++) {
    if (sum(rowMatrix1[index], colMatrix2[index]) < resultValue) {
      resultValue = sum(rowMatrix1[index], colMatrix2[index]);
    }
	}

	return resultValue;
}

template<typename T>
void getColMatrix(T& matrix, vector<int>& colMatrix, int colIndex) {
	for (int row = 0; row < numNodes; row++) {
		colMatrix[row] = matrix[row][colIndex];
	}
}

template<typename T>
T multMatrix(T& matrix1, T& matrix2) {
	vector<vector<int>> resultMatrix(numNodes, vector<int>(numNodes, 0));

	for (int row = 0; row < numNodes; row++) {
		for (int col = 0; col < numNodes; col++) {
			vector<int> rowMatrix1 = matrix1[row];
			vector<int> colMatrix2 = matrix1[row];

			getColMatrix(matrix2, colMatrix2, col);
			int resultProductPointMatrix = (int) dotProductMatrix(rowMatrix1, colMatrix2);

			resultMatrix[row][col] = resultProductPointMatrix;
		}
	}

	return resultMatrix;
}

template<typename T>
void printMatrix(T& matrix) {
  cout<<"Printing Matrix : \n";
  for(int row=0; row<numNodes; row++) {
    for(int col=0 ; col<numNodes; col++) {
      if (matrix[row][col] != INF) {
        cout<< matrix[row][col] <<" ";
      } else {
        cout<< "inf" <<" ";
      }
    }
    cout<<endl;
  }
  cout<<endl;
}

template<typename T>
T expMatrix(T& matrix, int exp) {
	if (exp == 0) {
		vector<vector<int>> identMatrix(numNodes, vector<int>(numNodes, 0));
		getIdentMatrix(identMatrix);
		return identMatrix;
	} else if (exp == 1) {
		return matrix;
	} else {
		return multMatrix(matrix, expMatrix(matrix, exp-1));
	}
}

int main() {

	printMatrix(convergenceMatrix);

	vector<vector<int>> resultMatrix(numNodes, vector<int>(numNodes, 0));
	resultMatrix = expMatrix(convergenceMatrix, numNodes);

	printMatrix(resultMatrix);

	return 0;
}
